package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.Frame;


public class FrameTest {

	@Test
	public void testSetFrameFirstThrowAsTwo() throws Exception{
		Frame frame=new Frame(2,0);
		assertEquals(2, frame.getFirstThrow());
	}
	
	@Test
	public void testSetFrameSecondThrowAsFour() throws Exception{
		Frame frame=new Frame(0,4);
		assertEquals(4, frame.getSecondThrow());
	}
	
	@Test
	public void testGetFrameScoreFirstThrowAsTwoAndSecondThrowAsSix() throws Exception{
		Frame frame=new Frame(2,6);
		assertEquals(8, frame.getScore());
	}
	

}
