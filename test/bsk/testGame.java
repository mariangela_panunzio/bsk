package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class testGame {

	private Game game=new Game();
	private Frame first;
	private Frame second;
	private Frame third;
	private Frame fourth;
	private Frame fifth;
	private Frame sixth;
	private Frame seventh;
	private Frame eighth;
	private Frame ninth;
	private Frame tenth;
	
	
	@Test
	public void testGetFirstFrameFromGame() throws Exception {
		first=new Frame(1,5);
		second=new Frame(3,6);
		third=new Frame(7,2);
		fourth=new Frame(3,6);
		fifth=new Frame(4,4);
		sixth=new Frame(5,3);
		seventh=new Frame(3,3);
		eighth=new Frame(4,5);
		ninth=new Frame(8,1);
		tenth=new Frame(2,6);
		
		game.addFrame(first);
		game.addFrame(second);
		game.addFrame(third);
		game.addFrame(fourth);
		game.addFrame(fifth);
		game.addFrame(sixth);
		game.addFrame(seventh);
		game.addFrame(eighth);
		game.addFrame(ninth);
		game.addFrame(tenth);
		
		
		assertEquals(first, game.getFrameAt(0));
	}
	
	@Test
	public void testGetSecondFrameFromGame() throws Exception {
		first=new Frame(1,5);
		second=new Frame(3,6);
		third=new Frame(7,2);
		fourth=new Frame(3,6);
		fifth=new Frame(4,4);
		sixth=new Frame(5,3);
		seventh=new Frame(3,3);
		eighth=new Frame(4,5);
		ninth=new Frame(8,1);
		tenth=new Frame(2,6);
		
		game.addFrame(first);
		game.addFrame(second);
		game.addFrame(third);
		game.addFrame(fourth);
		game.addFrame(fifth);
		game.addFrame(sixth);
		game.addFrame(seventh);
		game.addFrame(eighth);
		game.addFrame(ninth);
		game.addFrame(tenth);
		
		
		assertEquals(second, game.getFrameAt(1));
	}
	
	@Test
	public void testGetGameScore() throws Exception {
		first=new Frame(1,5);
		second=new Frame(3,6);
		third=new Frame(7,2);
		fourth=new Frame(3,6);
		fifth=new Frame(4,4);
		sixth=new Frame(5,3);
		seventh=new Frame(3,3);
		eighth=new Frame(4,5);
		ninth=new Frame(8,1);
		tenth=new Frame(2,6);
		
		game.addFrame(first);
		game.addFrame(second);
		game.addFrame(third);
		game.addFrame(fourth);
		game.addFrame(fifth);
		game.addFrame(sixth);
		game.addFrame(seventh);
		game.addFrame(eighth);
		game.addFrame(ninth);
		game.addFrame(tenth);
		
		
		assertEquals(81, game.calculateScore());
	}
	
	@Test
	public void testGetGameFirstFrameIsSpare() throws Exception {
		first=new Frame(1,9);
		second=new Frame(3,6);
		third=new Frame(7,2);
		fourth=new Frame(3,6);
		fifth=new Frame(4,4);
		sixth=new Frame(5,3);
		seventh=new Frame(3,3);
		eighth=new Frame(4,5);
		ninth=new Frame(8,1);
		tenth=new Frame(2,6);
		
		game.addFrame(first);
		game.addFrame(second);
		game.addFrame(third);
		game.addFrame(fourth);
		game.addFrame(fifth);
		game.addFrame(sixth);
		game.addFrame(seventh);
		game.addFrame(eighth);
		game.addFrame(ninth);
		game.addFrame(tenth);
		
		
		assertEquals(88, game.calculateScore());
	}
	
	@Test
	public void testGetGameFirstFrameIsStrike() throws Exception {
		first=new Frame(10,0);
		second=new Frame(3,6);
		third=new Frame(7,2);
		fourth=new Frame(3,6);
		fifth=new Frame(4,4);
		sixth=new Frame(5,3);
		seventh=new Frame(3,3);
		eighth=new Frame(4,5);
		ninth=new Frame(8,1);
		tenth=new Frame(2,6);
		
		game.addFrame(first);
		game.addFrame(second);
		game.addFrame(third);
		game.addFrame(fourth);
		game.addFrame(fifth);
		game.addFrame(sixth);
		game.addFrame(seventh);
		game.addFrame(eighth);
		game.addFrame(ninth);
		game.addFrame(tenth);
		
		
		assertEquals(94, game.calculateScore());
	}
	
	@Test
	public void testGetGameFirstFrameIsStrikeSecondFrameIsSpare() throws Exception {
		first=new Frame(10,0);
		second=new Frame(4,6);
		third=new Frame(7,2);
		fourth=new Frame(3,6);
		fifth=new Frame(4,4);
		sixth=new Frame(5,3);
		seventh=new Frame(3,3);
		eighth=new Frame(4,5);
		ninth=new Frame(8,1);
		tenth=new Frame(2,6);
		
		game.addFrame(first);
		game.addFrame(second);
		game.addFrame(third);
		game.addFrame(fourth);
		game.addFrame(fifth);
		game.addFrame(sixth);
		game.addFrame(seventh);
		game.addFrame(eighth);
		game.addFrame(ninth);
		game.addFrame(tenth);
		
		
		assertEquals(103, game.calculateScore());
	}
	
	@Test
	public void testGetGameFirstFrameIsStrikeSecondFrameIsStrike() throws Exception {
		first=new Frame(10,0);
		second=new Frame(10,0);
		third=new Frame(7,2);
		fourth=new Frame(3,6);
		fifth=new Frame(4,4);
		sixth=new Frame(5,3);
		seventh=new Frame(3,3);
		eighth=new Frame(4,5);
		ninth=new Frame(8,1);
		tenth=new Frame(2,6);
		
		game.addFrame(first);
		game.addFrame(second);
		game.addFrame(third);
		game.addFrame(fourth);
		game.addFrame(fifth);
		game.addFrame(sixth);
		game.addFrame(seventh);
		game.addFrame(eighth);
		game.addFrame(ninth);
		game.addFrame(tenth);
		
		
		assertEquals(112, game.calculateScore());
	}
	
	@Test
	public void testGetGameFirstFrameIsSpareSecondFrameIsSspare() throws Exception {
		first=new Frame(8,2);
		second=new Frame(5,5);
		third=new Frame(7,2);
		fourth=new Frame(3,6);
		fifth=new Frame(4,4);
		sixth=new Frame(5,3);
		seventh=new Frame(3,3);
		eighth=new Frame(4,5);
		ninth=new Frame(8,1);
		tenth=new Frame(2,6);
		
		game.addFrame(first);
		game.addFrame(second);
		game.addFrame(third);
		game.addFrame(fourth);
		game.addFrame(fifth);
		game.addFrame(sixth);
		game.addFrame(seventh);
		game.addFrame(eighth);
		game.addFrame(ninth);
		game.addFrame(tenth);
		
		
		assertEquals(98, game.calculateScore());
	}
	
	@Test
	public void testGetGameLastFrameIsSspare() throws Exception {
		first=new Frame(1,5);
		second=new Frame(3,6);
		third=new Frame(7,2);
		fourth=new Frame(3,6);
		fifth=new Frame(4,4);
		sixth=new Frame(5,3);
		seventh=new Frame(3,3);
		eighth=new Frame(4,5);
		ninth=new Frame(8,1);
		tenth=new Frame(2,8);
		
		game.addFrame(first);
		game.addFrame(second);
		game.addFrame(third);
		game.addFrame(fourth);
		game.addFrame(fifth);
		game.addFrame(sixth);
		game.addFrame(seventh);
		game.addFrame(eighth);
		game.addFrame(ninth);
		game.addFrame(tenth);
		
		game.setFirstBonusThrow(7);
		
		assertEquals(90, game.calculateScore());
	}
	
	@Test
	public void testGetGameLastFrameIsStrike() throws Exception {
		first=new Frame(1,5);
		second=new Frame(3,6);
		third=new Frame(7,2);
		fourth=new Frame(3,6);
		fifth=new Frame(4,4);
		sixth=new Frame(5,3);
		seventh=new Frame(3,3);
		eighth=new Frame(4,5);
		ninth=new Frame(8,1);
		tenth=new Frame(10,0);
		
		game.addFrame(first);
		game.addFrame(second);
		game.addFrame(third);
		game.addFrame(fourth);
		game.addFrame(fifth);
		game.addFrame(sixth);
		game.addFrame(seventh);
		game.addFrame(eighth);
		game.addFrame(ninth);
		game.addFrame(tenth);
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void testPerfectGame() throws Exception {
		first=new Frame(10,0);
		second=new Frame(10,0);
		third=new Frame(10,0);
		fourth=new Frame(10,0);
		fifth=new Frame(10,0);
		sixth=new Frame(10,0);
		seventh=new Frame(10,0);
		eighth=new Frame(10,0);
		ninth=new Frame(10,0);
		tenth=new Frame(10,0);
		
		game.addFrame(first);
		game.addFrame(second);
		game.addFrame(third);
		game.addFrame(fourth);
		game.addFrame(fifth);
		game.addFrame(sixth);
		game.addFrame(seventh);
		game.addFrame(eighth);
		game.addFrame(ninth);
		game.addFrame(tenth);
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		assertEquals(300, game.calculateScore());
	}
}
