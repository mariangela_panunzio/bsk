package tdd.training.bsk;

public class Game {

	private Frame frames[];
	private int indexFrame;
	private int firstBonusThrowFrame;
	private int secondBonusThrowFrame;
	
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frames=new Frame[10];
		indexFrame=0;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		frames[indexFrame]=frame;
		indexFrame++;
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return frames[index];	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		firstBonusThrowFrame=firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		secondBonusThrowFrame=secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrowFrame;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrowFrame;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score=0;
		
		for(int i=0; i<indexFrame; i++) {
			
			if(frames[i].isStrike())  {
				score+=calculateScoreStrike(i);
			}
			else if(frames[i].isSpare()){
				score+=calculateScoreSpare(i);
			}
			else {
				score+=frames[i].getFirstThrow()+frames[i].getSecondThrow();
			}
		}
		
		return score;
	}
	
	public int calculateScoreStrike(int i) {
		int score=0;
		
		if(i==indexFrame-2) {
			score+=frames[i].getFirstThrow()+frames[i+1].getFirstThrow()+this.getFirstBonusThrow();
		}
		else if(i==indexFrame-1) {
			score+=frames[i].getFirstThrow()+this.getFirstBonusThrow()+this.getSecondBonusThrow();
		}
		else if(frames[i+1].isStrike()) {
			score+=frames[i].getFirstThrow()+frames[i+1].getFirstThrow()+frames[i+2].getFirstThrow();
		}
		else {
			score+=frames[i].getFirstThrow()+frames[i+1].getFirstThrow()+frames[i+1].getSecondThrow();
		}
		
		return score;
	}
	
	public int calculateScoreSpare(int i) {
		int score=0;
		
		if(i==indexFrame-1) {
			score+=frames[i].getFirstThrow()+frames[i].getSecondThrow()+this.getFirstBonusThrow();
		}
		else {
			score+=frames[i].getFirstThrow()+frames[i].getSecondThrow()+frames[i+1].getFirstThrow();
		}
		
		return score;
	}

}
